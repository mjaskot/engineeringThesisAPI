export type LoginResponse = { accessToken: string; refreshToken: string };

export type JWTPayload = {
  [key: string]: string | number;
  iat: number;
  exp: number;
  uid: string;
};

export type PermissionsLevel = {
  [key: string]: number;
  admin: number;
  moderator: number;
  user: number;
};
