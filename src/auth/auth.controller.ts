import { Request, Response, NextFunction } from "express";

import { AuthService } from "./auth.service";

class AuthController {
  constructor(private readonly authService: AuthService) {}

  signIn = async (req: Request, res: Response, next: NextFunction) => {
    const { email, password } = req.body;
    try {
      return res.status(200).json({
        data: await this.authService.login(email, password)
      });
    } catch (err) {
      return next(err);
    }
  };

  register = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res.status(200).json({
        data: await this.authService.register(req.body)
      });
    } catch (err) {
      return next(err);
    }
  };
}

export const createAuthController = (
  authService: AuthService
): AuthController => {
  return new AuthController(authService);
};
