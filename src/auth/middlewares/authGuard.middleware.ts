import { Response, NextFunction } from "express";
import { Request } from "express-serve-static-core";

import { PermissionsLevel } from "../interfaces/customTypes";

const permissionsLevel: PermissionsLevel = {
  admin: 4,
  moderator: 2,
  user: 1
};

export const permit = (allowed: string) => {
  const isAllowed = (role: string) =>
    permissionsLevel[role] >= permissionsLevel[allowed];

  return (req: Request, res: Response, next: NextFunction) => {
    if (req.user && isAllowed(req.user.role)) {
      next();
    } else {
      res.status(403).json({ message: "Forbidden" });
    }
  };
};
