import { Request, Response, NextFunction } from "express";
import { UserModel } from "../../user/user.model";
import { User } from "user/interfaces/user.interface";
import { verifyToken } from "../helpers/token.helpers";
import { StatusError } from "../../misc/Errors";

declare global {
  namespace Express {
    interface Request {
      user: User;
    }
  }
}

const secret = process.env.SECRET;
if (!secret) {
  throw new Error("Secret is missing from environment variables.");
}

export const authorize = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.headers.authorization) {
    return res.status(401).json("Unauthorized");
  }

  if (req.headers.authorization) {
    try {
      const token = req.headers.authorization.split(" ")[1];
      const decoded = await verifyToken(token, secret);

      const user = await UserModel.findOne({
        _id: decoded.uid
      });

      if (!user) {
        throw new StatusError(404, "User not found");
      }

      req.user = user;

      return next();
    } catch (err) {
      return next(err);
    }
  }
};
