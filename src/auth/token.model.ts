import { prop, Typegoose } from "typegoose";

export class TokenSchema extends Typegoose {
  @prop({ required: true })
  userId: string;

  @prop({ required: true })
  token: string;
}

export const TokenModel = new TokenSchema().getModelForClass(TokenSchema, {
  schemaOptions: { timestamps: true, collection: "tokens" }
});
