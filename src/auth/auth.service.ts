import { Model } from "mongoose";
import { toRegisteredUser } from "./helpers/utils";
import { LoginResponse } from "./interfaces/customTypes";
import { hashPassword, checkPassword } from "./helpers/encryption.helpers";
import {
  UserRaw,
  RegisteredUser,
  CreateUserDTO
} from "../user/interfaces/user.interface";
import {
  generateAccessToken,
  generateRefreshToken
} from "./helpers/token.helpers";
import { StatusError } from "../misc/Errors";

export class AuthService {
  constructor(private readonly userModel: Model<UserRaw>) {}

  async register(params: CreateUserDTO): Promise<RegisteredUser> {
    const user = await this.userModel.create({
      ...params,
      password: await hashPassword(params.password, 10),
      role: "user"
    });

    const token = await generateAccessToken(user._id);

    const registeredUser: RegisteredUser = toRegisteredUser(user, token);

    return registeredUser;
  }

  async login(email: string, password: string): Promise<LoginResponse> {
    const user = await this.userModel.findOne({ email });

    const isValid = await checkPassword(password, user ? user.password : null);

    if (!user || !isValid) {
      throw new StatusError(401, "Your login or password is incorrect.");
    }

    const refreshToken = await generateRefreshToken(user._id);

    return {
      accessToken: await generateAccessToken(user._id),
      refreshToken: refreshToken.token
    };
  }
}
// `refreshToken: refreshToken => {}
// resetPassword: email => {}`

export const createAuthService = (UserModel: Model<UserRaw>) =>
  new AuthService(UserModel);
