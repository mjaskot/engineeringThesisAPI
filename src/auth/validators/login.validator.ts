import { celebrate, Joi } from "celebrate";

export const createLoginValidator = () =>
  celebrate({
    body: Joi.object()
      .keys({
        email: Joi.string()
          .email()
          .min(2)
          .max(30)
          .required(),
        password: Joi.string()
          .min(5)
          .max(18)
          .required()
      })
      .required()
  });
