import { celebrate, Joi } from "celebrate";

export const createRegisterValidator = () =>
  celebrate({
    body: Joi.object()
      .keys({
        username: Joi.string()
          .min(3)
          .max(25)
          .required(),
        name: Joi.string()
          .min(3)
          .max(20),
        lastName: Joi.string()
          .min(3)
          .max(20),
        age: Joi.number()
          .positive()
          .min(16)
          .required(),
        email: Joi.string()
          .email()
          .min(2)
          .max(30)
          .required(),
        address: Joi.string()
          .min(3)
          .max(30),
        city: Joi.string()
          .min(2)
          .max(30),
        country: Joi.string()
          .min(4)
          .max(30),
        about: Joi.string().max(300),
        password: Joi.string()
          .min(5)
          .max(18)
          .required()
      })
      .required()
  });

export const createPutUserValidator = () =>
  celebrate({
    body: Joi.object().keys({
      username: Joi.string()
        .min(3)
        .max(25),
      name: Joi.string()
        .min(3)
        .max(20),
      lastName: Joi.string()
        .min(3)
        .max(20),
      age: Joi.number()
        .positive()
        .min(16),
      email: Joi.string()
        .email()
        .min(2)
        .max(30),
      address: Joi.string()
        .min(3)
        .max(30),
      city: Joi.string()
        .min(2)
        .max(30),
      country: Joi.string()
        .min(4)
        .max(30),
      about: Joi.string().max(300),
      password: Joi.string()
        .min(5)
        .max(18)
    })
  });
