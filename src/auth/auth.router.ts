import { compose } from "ramda";
import { Router } from "express";

import { createAuthController } from "./auth.controller";
import { createAuthService } from "./auth.service";
import { UserModel } from "../user/user.model";
import { createLoginValidator } from "./validators/login.validator";
import { createRegisterValidator } from "./validators/user.validator";

export const createAuthRouter = () => {
  const authRouter = Router();

  const authController = compose(
    createAuthController,
    createAuthService
  )(UserModel);

  authRouter
    .route("/register")
    .post(createRegisterValidator(), authController.register);
  authRouter
    .route("/login")
    .post(createLoginValidator(), authController.signIn);

  return authRouter;
};
