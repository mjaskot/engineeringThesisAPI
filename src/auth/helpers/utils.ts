import { omit } from "ramda";
import {
  UserRaw,
  UserResponse,
  User,
  RegisteredUser
} from "user/interfaces/user.interface";

export const toRegisteredUser = (
  userRaw: UserRaw,
  token: string
): RegisteredUser => {
  const registeredUser: RegisteredUser = {
    username: userRaw.username,
    name: userRaw.name,
    lastName: userRaw.lastName,
    age: userRaw.age,
    email: userRaw.email,
    adress: userRaw.adress,
    city: userRaw.city,
    country: userRaw.country,
    about: userRaw.about,
    accessToken: { token }
  };

  return registeredUser;
};

export const formatUserResponse = (user: User): UserResponse => {
  return {
    ...omit(["password", "role"], user)
  };
};
