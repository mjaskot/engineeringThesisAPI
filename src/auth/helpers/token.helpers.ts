import { sign, verify } from "jsonwebtoken";
import { TokenModel } from "../token.model";
import { JWTPayload } from "../interfaces/customTypes";
import { StatusError } from "../../misc/Errors";

const secret = process.env.SECRET;
if (!secret) {
  throw new Error("Secret is missing from environmental variables");
}

export const generateAccessToken = (userId: string) => {
  const token: JWTPayload = {
    iat: Math.floor(Date.now() / 1000),
    exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 30,
    uid: userId
  };

  return sign(token, secret);
};

export const generateRefreshToken = async (userId: string) => {
  const token: JWTPayload = {
    iat: Math.floor(Date.now() / 1000),
    exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 30,
    uid: userId
  };

  const foundToken = await TokenModel.findOne({ userId });

  if (!foundToken) {
    return await TokenModel.create({
      userId: userId,
      token: await sign(token, secret)
    });
  }

  await foundToken.update({
    $set: {
      token: await sign(token, secret)
    }
  });

  return foundToken;
};

export const verifyToken = (
  token: string,
  secret: string
): Promise<JWTPayload> => {
  return new Promise((resolve, reject) => {
    verify(token, secret, (err, decoded) => {
      if (err) {
        reject(new StatusError(401, err.message));
      }
      resolve(decoded as JWTPayload);
    });
  });
};
