import express = require("express");
import bodyParser = require("body-parser");
import { errors } from "celebrate";
import { Request, Response, NextFunction } from "express";

import { createPostRouter } from "./posts/post.router";
import { createBoardRouter } from "./boards/board.router";
import { createUserRouter } from "./user/user.router";
import { createAuthRouter } from "./auth/auth.router";
import { StatusError } from "misc/Errors";

const app = express();

app.use(bodyParser.json());
app.use("/boards", createBoardRouter());
app.use("/posts", createPostRouter());
app.use("/users", createUserRouter());
app.use("/auth", createAuthRouter());
app.use(errors());
app.use(
  (err: StatusError, _req: Request, res: Response, _next: NextFunction) => {
    res.status(err.status).json({ message: err.message });
  }
);

export default app;
