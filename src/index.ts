require("dotenv").config();

import app from "./app";

import { createDBconnection } from "./db";

const mongoUrl = process.env.DB_URL;
const dbName = process.env.DB_NAME;
const port = process.env.PORT;

const bootstrap = async () => {
  try {
    if (!mongoUrl) {
      throw new Error("Database url is not provided");
    }
    if (!dbName) {
      throw new Error("Database name is not specified");
    }

    createDBconnection(mongoUrl, dbName);
    app.listen(port, () => console.log(`Server listening on port ${port}`));
  } catch (err) {
    console.log(err);
  }
};

bootstrap();
