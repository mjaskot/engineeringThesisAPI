import { ObjectId } from "mongodb";

import { User, UserRaw, UserResponse } from "./interfaces/user.interface";
import { Model } from "mongoose";

export class UserService {
  constructor(private readonly userModel: Model<UserRaw>) {}

  getAll(): Promise<UserResponse[]> {
    return this.userModel
      .find({}, { password: 0 })
      .lean()
      .exec();
  }

  async getOne(userId: string): Promise<User> {
    const user = await this.userModel
      .findOne({ _id: new ObjectId(userId) })
      .exec();

    if (!user) {
      throw new Error("User not Found");
    }

    return user;
  }

  async editOne(userId: string, params: User): Promise<User> {
    const user = await this.userModel
      .findOneAndUpdate({ _id: new ObjectId(userId) }, { $set: params })
      .exec();

    if (!user) {
      throw new Error("User not found.");
    }

    return user;
  }

  async deleteOne(userId: string): Promise<User> {
    const user = await this.userModel
      .findByIdAndRemove({ _id: new ObjectId(userId) })
      .exec();

    if (!user) {
      throw new Error("User not found.");
    }

    return user;
  }
}

export const createUserService = (userModel: Model<UserRaw>) =>
  new UserService(userModel);
