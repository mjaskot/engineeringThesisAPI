import { Router } from "express";
import { compose } from "ramda";

import { UserModel } from "./user.model";
import { createUserService } from "./user.service";
import { createUserController } from "./user.controller";

export const createUserRouter = () => {
  const userRouter = Router();

  const userController = compose(
    createUserController,
    createUserService
  )(UserModel);

  userRouter.route("/").get(userController.getAllUsers);

  userRouter
    .route("/:userId")
    .get(userController.getUser)
    .put(userController.putUser)
    .delete(userController.removeUser);

  return userRouter;
};
