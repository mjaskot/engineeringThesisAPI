import { prop, Typegoose } from "typegoose";

import Roles from "./interfaces/roles";

export class UserSchema extends Typegoose {
  @prop({ required: true })
  username: string;

  @prop()
  name: string;

  @prop()
  lastName: string;

  @prop({ required: true })
  age: number;

  @prop({ unique: true, required: true })
  email: string;

  @prop()
  address: string;

  @prop()
  city: string;

  @prop()
  country: string;

  @prop()
  about: string;

  @prop({ required: true })
  password: string;

  @prop({ enum: Roles, default: Roles.USER })
  role: string | "user";
}

export const UserModel = new UserSchema().getModelForClass(UserSchema, {
  schemaOptions: { collection: "users", timestamps: true }
});
