enum UserPermissions {
  CREATE_USER = "user:create",
  READ_USER = "user:read",
  UPDATE_USER = "user:update",
  REMOVE_USER = "user:remove"
}
