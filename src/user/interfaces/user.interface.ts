import { Document } from "mongoose";

export type User = {
  username: string;
  name?: string;
  lastName?: string;
  age?: number;
  email: string;
  adress?: string;
  city?: string;
  country?: string;
  about?: string;
  password: string;
  role: string;
};

export type CreateUserDTO = {
  username: string;
  name?: string;
  lastName?: string;
  age?: number;
  email: string;
  adress?: string;
  city?: string;
  country?: string;
  about?: string;
  password: string;
};

export type UserRaw = Document & User;

export type UserResponse = {
  username: string;
  name?: string;
  lastName?: string;
  age?: number;
  email: string;
  adress?: string;
  city?: string;
  country?: string;
  about?: string;
};

export type RegisteredUser = {
  username: string;
  name?: string;
  lastName?: string;
  age?: number;
  email: string;
  adress?: string;
  city?: string;
  country?: string;
  about?: string;
  accessToken: {
    token: string;
  };
};
