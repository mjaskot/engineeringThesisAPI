import { UserService } from "./user.service";
import { Request, Response, NextFunction } from "express";

export class UserController {
  constructor(private readonly userService: UserService) {}

  getAllUsers = async (_req: Request, res: Response, next: NextFunction) => {
    try {
      return res.status(200).json(await this.userService.getAll());
    } catch (err) {
      return next(err);
    }
  };

  getUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(200)
        .json(await this.userService.getOne(req.params.userId));
    } catch (err) {
      return next(err);
    }
  };

  putUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(202)
        .json(await this.userService.editOne(req.params.userId, req.body));
    } catch (err) {
      return next(err);
    }
  };

  removeUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(204)
        .json(await this.userService.deleteOne(req.params.userId));
    } catch (err) {
      return next(err);
    }
  };
}

export const createUserController = (userService: UserService) =>
  new UserController(userService);
