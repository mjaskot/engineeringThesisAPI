import { Request, Response, NextFunction } from "express";

import { PostService } from "./post.service";

export class PostController {
  constructor(private readonly postService: PostService) {}

  getAllPosts = async (_req: Request, res: Response, next: NextFunction) => {
    try {
      return res.status(200).json(await this.postService.getAll());
    } catch (err) {
      return next(err);
    }
  };

  getPost = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(200)
        .json(await this.postService.getOne(req.params.postId));
    } catch (err) {
      return next(err);
    }
  };

  createPost = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res.status(200).json(await this.postService.createNew(req.body));
    } catch (err) {
      return next(err);
    }
  };

  editPost = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(202)
        .json(await this.postService.edit(req.params.postId, req.body));
    } catch (err) {
      return next(err);
    }
  };

  deletePost = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(200)
        .json(await this.postService.deleteOne(req.params.postId));
    } catch (err) {
      return next(err);
    }
  };
}

export const createPostController = (postService: PostService) =>
  new PostController(postService);
