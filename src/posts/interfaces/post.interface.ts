import { Document } from "mongoose";
import { Comment } from "./../comments/interfaces/comment.interface";

export type Post = {
  title: string;
  subtitle: string;
  content: string;
  karma: number;
  tags: string[];
  comments: Comment[];
};

export type PostRaw = Document & Post;

export type PostResponse = {
  title: string;
  subtitle: string;
  content: string;
  karma: number;
  tags: string[];
  comments: Comment[];
};
