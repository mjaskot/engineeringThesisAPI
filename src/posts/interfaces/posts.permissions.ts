enum BoardsPermissions {
  READ_POST = "post:read",
  CREATE_POST = "post:create",
  REMOVE_POST = "post:remove",
  UPDATE_POST = "post:update"
}
