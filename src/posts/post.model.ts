import { prop, Typegoose, arrayProp } from "typegoose";
import { Comment } from "./comments/interfaces/comment.interface";

class CommentSchema extends Typegoose {
  @prop({ required: true })
  content: string;

  @prop({ required: true })
  postedBy: string;
}

export class PostSchema extends Typegoose {
  @prop({ required: true })
  title: string;

  @prop()
  subtitle: string;

  @prop()
  content: string;

  @prop()
  karma: number;

  @prop()
  tags: Array<string>;

  @arrayProp({ items: CommentSchema })
  comments: Comment[];
}

export const PostModel = new PostSchema().getModelForClass(PostSchema, {
  schemaOptions: { timestamps: true, collection: "posts" }
});
