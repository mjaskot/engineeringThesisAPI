import { ObjectId } from "mongodb";
import { Model } from "mongoose";

import { PostRaw, Post } from "./interfaces/post.interface";

export class PostService {
  constructor(private readonly postModel: Model<PostRaw>) {}

  getAll() {
    return this.postModel
      .find({})
      .lean()
      .exec();
  }
  getOne(postId: number) {
    return this.postModel.findOne({ _id: new ObjectId(postId) });
  }

  createNew(params: Post): Promise<Post> {
    return this.postModel.create(params);
  }

  edit(postId: number, params: {}) {
    return this.postModel.findOneAndUpdate(
      { _id: new ObjectId(postId) },
      { $set: params }
    );
  }
  deleteOne(postId: number) {
    return this.postModel.findByIdAndRemove({ _id: new ObjectId(postId) });
  }
}

export const createPostService = (postModel: Model<PostRaw>): PostService => {
  return new PostService(postModel);
};
