import { Router } from "express";
import { compose } from "ramda";

import { PostModel } from "./post.model";
import { createPostService } from "./post.service";
import { createPostController } from "./post.controller";
import { createCommentRouter } from "./comments/comment.router";

export const createPostRouter = () => {
  const postRouter = Router();
  const commentRouter = createCommentRouter();

  const postController = compose(
    createPostController,
    createPostService
  )(PostModel);

  postRouter.use(commentRouter);
  postRouter
    .route("/")
    .get(postController.getAllPosts)
    .post(postController.createPost);

  postRouter
    .route("/:postId")
    .get(postController.getPost)
    .put(postController.editPost)
    .delete(postController.deletePost);

  return postRouter;
};
