import { celebrate, Joi } from "celebrate";

export const createAddPostValidatorFactory = () =>
  celebrate({
    body: Joi.object().keys({
      title: Joi.string().required(),
      content: Joi.string().required(),
      tags: Joi.array()
        .items(Joi.string())
        .optional()
    })
  });

export const createUpdatePostValidatorFactory = () =>
  celebrate({
    body: Joi.object().keys({
      title: Joi.string().optional(),
      content: Joi.string().optional(),
      tags: Joi.array()
        .items(Joi.string())
        .optional()
    })
  });
