import { Request, Response, NextFunction } from "express";
import { CommentService } from "./comment.service";

export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  postComment = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(201)
        .json(await this.commentService.createNew(req.params.postId, req.body));
    } catch (err) {
      return next(err);
    }
  };

  putComment = async (req: Request, res: Response, next: NextFunction) => {
    const { postId, commentId } = req.params;
    try {
      return res
        .status(201)
        .json(await this.commentService.editOne(postId, commentId, req.body));
    } catch (err) {
      return next(err);
    }
  };

  deleteComment = async (req: Request, res: Response, next: NextFunction) => {
    const { postId, commentId } = req.params;
    try {
      return res
        .status(202)
        .json(await this.commentService.deleteOne(postId, commentId));
    } catch (err) {
      return next(err);
    }
  };
}

export const createCommentController = (commentService: CommentService) =>
  new CommentController(commentService);
