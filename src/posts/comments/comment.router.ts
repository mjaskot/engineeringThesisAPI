import { Router } from "express";
import { compose } from "ramda";

import { PostModel } from "../post.model";
import { createCommentService } from "./comment.service";
import { createCommentController } from "./comment.controller";

export const createCommentRouter = (): Router => {
  const commentRouter = Router();

  const commentController = compose(
    createCommentController,
    createCommentService
  )(PostModel);

  commentRouter.route("/:postId/comment").post(commentController.postComment);

  commentRouter
    .route("/:postId/comment/:commentId")
    .put(commentController.putComment)
    .delete(commentController.deleteComment);

  return commentRouter;
};
