import { ObjectId } from "mongodb";
import { Model } from "mongoose";

import { Comment } from "./interfaces/comment.interface";
import { PostRaw } from "../interfaces/post.interface";

type TransformedParams = { [key: string]: string };

export class CommentService {
  constructor(private readonly postModel: Model<PostRaw>) {}

  createNew(postId: string, params: Comment) {
    return this.postModel.findOneAndUpdate(
      { _id: new ObjectId(postId) },
      { $push: { comments: { _id: new ObjectId(), ...params } } }
    );
  }

  transformParams = (params: Comment): TransformedParams => {
    return Object.entries(params).reduce((prev, [key, value]) => {
      return {
        ...prev,
        [`comments.$.${key}`]: value
      };
    }, {});
  };

  editOne(postId: string, commentId: string, params: Comment) {
    const transformedParams = this.transformParams(params);

    return this.postModel.findOneAndUpdate(
      { _id: new ObjectId(postId), "comments._id": new ObjectId(commentId) },
      { $set: transformedParams }
    );
  }

  deleteOne(postId: string, commentId: string) {
    return this.postModel.findOneAndUpdate(
      { _id: new ObjectId(postId) },
      { $pull: { comments: { _id: new ObjectId(commentId) } } }
    );
  }
}

export const createCommentService = (postModel: Model<PostRaw>) =>
  new CommentService(postModel);

// DB <- PROVIDER <- (SERVICE) <- CONTROLLER <- VALIDATOR <- ROUTER <- EXPRESS <- REQUEST.
// DB -> PROVIDER -> (SERVICE) -> CONTROLER -> EXPRESS -> RESPONSE
