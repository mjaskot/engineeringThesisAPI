import { Document } from "mongoose";

export type Comment = {
  [key: string]: string;
  content: string;
  postedBy: string;
};

export type CommentResponse = {
  content: string;
  postedBy: string;
};

export type CommentRaw = Document & Comment;
