import { celebrate, Joi } from "celebrate";

const createAddCommentValidatorFactory = () =>
  celebrate({
    body: Joi.object().keys({
      content: Joi.string().required()
    })
  });

const createUpdateCommentValidatorFactory = () =>
  celebrate({
    body: Joi.object().keys({
      content: Joi.string()
    })
  });

module.exports = {
  createAddCommentValidatorFactory,
  createUpdateCommentValidatorFactory
};
