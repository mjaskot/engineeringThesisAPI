import { ObjectId } from "bson";
import { BoardRaw } from "./interfaces/board.interface";
import { Model } from "mongoose";

export class BoardService {
  constructor(private readonly boardModel: Model<BoardRaw>) {}
  getAll() {
    return this.boardModel
      .find({})
      .lean()
      .exec();
  }

  getOne(boardId: string) {
    return this.boardModel.findOne({ _id: new ObjectId(boardId) });
  }

  createNew(params: Object) {
    return this.boardModel.create(params);
  }

  edit(boardId: string, params: Object) {
    return this.boardModel.findOneAndUpdate(
      { _id: new ObjectId(boardId) },
      { $set: params }
    );
  }

  deleteOne(boardId: string) {
    return this.boardModel.findByIdAndRemove({ _id: new ObjectId(boardId) });
  }
}

export const createBoardService = (boardModel: Model<BoardRaw>) =>
  new BoardService(boardModel);
