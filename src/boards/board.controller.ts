import { Request, Response, NextFunction } from "express";

import { BoardService } from "./board.service";

class BoardController {
  constructor(private readonly boardService: BoardService) {}

  getAllBoards = async (_req: Request, res: Response, next: NextFunction) => {
    try {
      return res.status(200).json(await this.boardService.getAll());
    } catch (err) {
      return next(err);
    }
  };

  getBoard = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(200)
        .json(await this.boardService.getOne(req.params.boardId));
    } catch (err) {
      return next(err);
    }
  };

  createBoard = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res.status(201).json(await this.boardService.createNew(req.body));
    } catch (err) {
      return next(err);
    }
  };

  async editBoard(req: Request, res: Response, next: NextFunction) {
    try {
      return res
        .status(202)
        .json(await this.boardService.edit(req.params.boardId, req.body));
    } catch (err) {
      return next(err);
    }
  }

  deleteBoard = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(204)
        .json(await this.boardService.deleteOne(req.params.boardId));
    } catch (err) {
      return next(err);
    }
  };
}

export const createBoardController = (boardService: BoardService) =>
  new BoardController(boardService);
