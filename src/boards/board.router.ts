import { Router } from "express";
import { compose } from "ramda";

import { BoardModel } from "./board.model";
import { createBoardController } from "./board.controller";
import { createBoardService } from "./board.service";
import { authorize } from "../auth/middlewares/authorize.middleware";
import { permit } from "../auth/middlewares/authGuard.middleware";

export const createBoardRouter = () => {
  const boardRouter = Router();

  const boardController = compose(
    createBoardController,
    createBoardService
  )(BoardModel);

  boardRouter
    .route("/")
    .get(boardController.getAllBoards)
    .post(authorize, permit("user"), boardController.createBoard);

  boardRouter
    .route("/:boardId")
    .get(authorize, permit("user"), boardController.getBoard)
    .put(authorize, permit("user"), boardController.editBoard)
    .delete(authorize, permit("user"), boardController.deleteBoard);

  return boardRouter;
};
