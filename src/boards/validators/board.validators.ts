const { celebrate, Joi } = require("celebrate");

const createAddBoardValidatorFactory = () =>
  celebrate({
    body: Joi.object().keys({
      name: Joi.string().required(),
      description: Joi.string().required()
    })
  });

const createUpdateBoardValidatorFactory = () =>
  celebrate({
    body: Joi.object().keys({
      name: Joi.string(),
      description: Joi.string()
    })
  });

module.exports = {
  createAddBoardValidatorFactory,
  createUpdateBoardValidatorFactory
};
