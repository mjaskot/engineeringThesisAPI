import { prop, Typegoose } from "typegoose";
import { Post } from "../posts/interfaces/post.interface";

export class BoardSchema extends Typegoose {
  @prop({ required: true })
  name: string;

  @prop()
  description: string;

  @prop()
  posts: Post[];

  @prop()
  postsAmount: number;

  @prop()
  tags: Array<string>;
}

export const BoardModel = new BoardSchema().getModelForClass(BoardSchema, {
  schemaOptions: { timestamps: true, collection: "boards" }
});
