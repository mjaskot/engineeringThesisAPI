enum BoardsPermissions {
  READ_BOARD = "board:read",
  CREATE_BOARD = "board:create",
  REMOVE_BOARD = "board:remove",
  UPDATE_BOARD = "board:update"
}
