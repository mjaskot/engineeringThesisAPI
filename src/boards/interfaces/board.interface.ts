import { Document } from "mongoose";
import { Post } from "../../posts/interfaces/post.interface";

export type Board = {
  name: string;
  description: string;
  posts: Post[];
  postsAmount: number;
  tags: string[];
};

export type BoardRaw = Document & Board;

export type BoardResponse = {
  name: string;
  description: string;
  posts: Post[];
  postsAmount: number;
  tags: string[];
};
