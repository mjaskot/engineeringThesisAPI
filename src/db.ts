import { connect } from "mongoose";

export const createDBconnection = (
  mongoUrl: string,
  dbName: string
): Promise<Boolean> => {
  return new Promise((resolve, reject) => {
    const connectionLink = `mongodb://${mongoUrl}:27017/${dbName}`;
    try {
      connect(
        connectionLink,
        { useNewUrlParser: true },
        () => {
          console.log("Database connected!");
          resolve(true);
        }
      );
    } catch (err) {
      console.error(err.stack);
      reject(false);
    }
  });
};
