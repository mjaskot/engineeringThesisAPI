# Engineering Thesis API

## :arrow_up: How to run

**Step 1** clone this repository
**Step 2** In cli cd to this repository
**Step 3** Install all packages with `yarn` or `npm i` command
**Step 4** Run project by using `yarn start` or `yarn watch` for nodemon

The `.env` file is ignored by git

## Get started:

1.  Copy .env.example to .env
2.  Add your config variables
3.  Done!
